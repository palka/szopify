package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived;

import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

@Module
class ArchivedListDetailsModule {

    private final ArchivedListDetailsContract.View mView;
    private final ShoppingList mShoppingList;

    ArchivedListDetailsModule(ArchivedListDetailsContract.View view, ShoppingList shoppingList) {
        this.mView = view;
        this.mShoppingList = shoppingList;
    }

    @Provides
    BaseSchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }

    @Provides
    ArchivedListDetailsContract.View providesView() {
        return mView;
    }

    @Provides
    ShoppingList providesShoppingList() {
        return mShoppingList;
    }
}
