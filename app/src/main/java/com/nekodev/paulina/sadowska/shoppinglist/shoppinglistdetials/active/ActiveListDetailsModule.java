package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active;

import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

@Module
class ActiveListDetailsModule {

    private final ActiveListDetailsContract.View mView;
    private final ShoppingList mShoppingList;

    ActiveListDetailsModule(ActiveListDetailsContract.View view, ShoppingList shoppingList) {
        this.mView = view;
        this.mShoppingList = shoppingList;
    }

    @Provides
    BaseSchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }

    @Provides
    ActiveListDetailsContract.View providesView() {
        return mView;
    }

    @Provides
    ShoppingList providesShoppingList() {
        return mShoppingList;
    }
}