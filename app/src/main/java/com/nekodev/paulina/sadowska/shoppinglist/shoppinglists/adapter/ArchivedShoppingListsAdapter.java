package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter;

import com.nekodev.paulina.sadowska.shoppinglist.R;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */

public class ArchivedShoppingListsAdapter extends ShoppingListsAdapter {

    public ArchivedShoppingListsAdapter(OnItemClickedListener onItemClickedListener) {
        super(onItemClickedListener);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.partial_archived_list_item;
    }
}
