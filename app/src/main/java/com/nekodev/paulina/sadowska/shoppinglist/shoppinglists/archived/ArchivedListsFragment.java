package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.archived;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived.ArchivedListDetailsActivity;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.BaseShopListsFragment;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter.ArchivedShoppingListsAdapter;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter.ShoppingListsAdapter;

import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

public class ArchivedListsFragment extends BaseShopListsFragment<ArchivedListsContract.Presenter> implements ArchivedListsContract.View {

    public static ArchivedListsFragment newInstance() {
        return new ArchivedListsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basic, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onUndoClicked(ShoppingList shoppingList, int position) {
        mPresenter.archiveItem(shoppingList, position);
    }

    @Override
    public void showListDetails(ShoppingList shoppingList) {
        startActivity(ArchivedListDetailsActivity.getStartIntent(getContext(), shoppingList));
    }

    @Override
    protected String getUndoMessage(String name) {
        return getResources().getString(R.string.unarchived_message, name);
    }

    @Override
    protected void onItemSwiped(ShoppingList shoppingList, int position, boolean isLastItem) {
        mPresenter.unarchiveItem(shoppingList, position, isLastItem);
    }

    @Override
    protected ShoppingListsAdapter createAdapter() {
        return new ArchivedShoppingListsAdapter(shoppingList -> mPresenter.onListClicked(shoppingList));
    }
}
