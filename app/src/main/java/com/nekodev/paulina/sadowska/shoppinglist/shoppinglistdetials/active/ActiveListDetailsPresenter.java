package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingDataSource;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepository;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ActiveListDetailsPresenter implements ActiveListDetailsContract.Presenter {

    private final ActiveListDetailsContract.View mView;
    private final ShoppingDataSource mShoppingRepository;
    private final BaseSchedulerProvider mSchedulerProvider;
    private final CompositeDisposable mDisposable;
    private final ShoppingList mShoppingList;

    @Inject
    ActiveListDetailsPresenter(@NonNull ActiveListDetailsContract.View view,
                               @NonNull ShoppingRepository shoppingRepository,
                               @NonNull BaseSchedulerProvider schedulerProvider,
                               @NonNull ShoppingList shoppingList) {
        this.mView = view;
        this.mShoppingRepository = shoppingRepository;
        this.mSchedulerProvider = schedulerProvider;
        this.mShoppingList = shoppingList;
        this.mDisposable = new CompositeDisposable();
    }

    @Inject
    void setupListeners() {
        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        fetchListDetails();
    }

    private void fetchListDetails() {
        mDisposable.add(mShoppingRepository.getListItems(mShoppingList.getId())
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        mView::showItems,
                        e -> mView.showErrorView(R.string.error_fetching_list_details)
                ));
    }

    @Override
    public void unSubscribe() {
        mDisposable.dispose();
    }

    @Override
    public void addItemToBuy(String name) {
        if (TextUtils.isEmpty(name)) {
            mView.showError(R.string.error_item_name_empty);
            return;
        }
        mDisposable.add(mShoppingRepository.addItem(name, mShoppingList.getId())
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        mView::addItemOnEnd,
                        e -> mView.showError(R.string.error_creating_item)
                ));
    }

    @Override
    public void deleteItem(ItemToBuyDb item, int position) {
        mDisposable.add(mShoppingRepository.removeItem(item)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        removedItem -> {
                            mView.removeItem(position);
                            mView.showUndoSnackbar(removedItem, position);
                        },
                        e -> mView.showError(R.string.error_deleting_item)
                ));
    }

    @Override
    public void onUndoClicked(ItemToBuyDb item, int position) {
        mDisposable.add(mShoppingRepository.restoreItem(item)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        restoredItem -> mView.addItemAtPosition(restoredItem, position),
                        e -> mView.showError(R.string.error_restore_item)
                ));
    }

    @Override
    public void onEditClicked(ItemToBuyDb itemToBuy, int position) {
        mView.showEditDialog(itemToBuy, position);
    }

    @Override
    public void onCheckChanged(ItemToBuyDb itemToBuy, boolean isChecked) {
        itemToBuy.setChecked(isChecked);
        editItem(itemToBuy, -1);
    }

    @Override
    public void onNameChanged(String newName, ItemToBuyDb itemToBuy, int position) {
        itemToBuy.setName(newName);
        editItem(itemToBuy, position);
    }

    private void editItem(ItemToBuyDb itemToBuy, int positionToRefresh) {
        mDisposable.add(mShoppingRepository.editItem(itemToBuy)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        editedItem -> {
                            if (positionToRefresh >= 0) {
                                mView.refreshItem(editedItem, positionToRefresh);
                            }
                        },
                        e -> mView.showError(R.string.error_edit_item)
                ));
    }
}
