package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.BaseItemsToBuyAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ArchivedItemsToBuyAdapter extends BaseItemsToBuyAdapter<ArchivedItemsToBuyAdapter.ViewHolder> {
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.partial_item_to_buy, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(itemsToBuy.get(position));
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_item_to_buy)
        TextView mName;
        @BindView(R.id.checkbox_item_to_buy)
        CheckBox mCheckbox;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(ItemToBuyDb itemToBuy) {
            mCheckbox.setChecked(itemToBuy.isChecked());
            mCheckbox.setEnabled(false);
            mName.setText(itemToBuy.getName());
        }
    }
}
