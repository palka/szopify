package com.nekodev.paulina.sadowska.shoppinglist;

import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListConverter;

import java.text.SimpleDateFormat;
import java.util.Locale;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

@Module
public final class ApplicationModule {

    @Provides
    ShoppingListConverter providesShoppingListDbConverter(SimpleDateFormat dateFormat) {
        return new ShoppingListConverter(dateFormat);
    }

    @Provides
    SimpleDateFormat providesDisplayedDateFormat() {
        return new SimpleDateFormat("dd MMMM HH:mm", Locale.getDefault());
    }
}

