package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived;

import com.nekodev.paulina.sadowska.shoppinglist.BasePresenter;
import com.nekodev.paulina.sadowska.shoppinglist.BaseView;
import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;

import java.util.List;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public interface ArchivedListDetailsContract {
    interface View extends BaseView<Presenter> {
        void showItems(List<ItemToBuyDb> itemsToBuy);
    }

    interface Presenter extends BasePresenter {

    }
}