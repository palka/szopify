package com.nekodev.paulina.sadowska.shoppinglist;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public interface BasePresenter {
    void subscribe();

    void unSubscribe();
}
