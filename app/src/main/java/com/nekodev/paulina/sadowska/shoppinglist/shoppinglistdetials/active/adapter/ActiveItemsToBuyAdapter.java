package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.BaseItemsToBuyAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ActiveItemsToBuyAdapter extends BaseItemsToBuyAdapter<ActiveItemsToBuyAdapter.ViewHolder> {

    private OnEditClickedListener mEditListener;
    private OnCheckChangedListener mCheckChangedListener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.partial_item_to_buy, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(itemsToBuy.get(position));
    }

    public void addItem(ItemToBuyDb itemToBuy, int position) {
        itemsToBuy.add(position, itemToBuy);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        itemsToBuy.remove(position);
        notifyItemRemoved(position);
    }

    public ItemToBuyDb getItem(int position) {
        return itemsToBuy.get(position);
    }

    public void replaceItem(ItemToBuyDb editedItem, int position) {
        itemsToBuy.remove(position);
        itemsToBuy.add(position, editedItem);
        notifyItemChanged(position);
    }

    public void setOnEditListener(OnEditClickedListener mEditListener) {
        this.mEditListener = mEditListener;
    }

    public void setOnCheckChangedListener(OnCheckChangedListener mCheckChangedListener) {
        this.mCheckChangedListener = mCheckChangedListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.details_foreground)
        View foregroundLayout;
        @BindView(R.id.button_edit_item_to_buy)
        ImageView mEditButton;
        @BindView(R.id.text_item_to_buy)
        TextView mName;
        @BindView(R.id.checkbox_item_to_buy)
        CheckBox mCheckbox;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(ItemToBuyDb itemToBuy) {
            mCheckbox.setChecked(itemToBuy.isChecked());
            mName.setText(itemToBuy.getName());
            mEditButton.setVisibility(View.VISIBLE);
            mEditButton.setOnClickListener(v -> {
                if (mEditListener != null) {
                    mEditListener.onEditClicked(itemToBuy, getAdapterPosition());
                }
            });
            mCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (mCheckChangedListener != null) {
                    mCheckChangedListener.onCheckChanged(isChecked, getAdapterPosition());
                }
            });
        }
    }

    public interface OnEditClickedListener {
        void onEditClicked(ItemToBuyDb itemToBuyDb, int position);
    }

    public interface OnCheckChangedListener {
        void onCheckChanged(boolean isChecked, int position);
    }
}
