package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.active;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active.ActiveListDetailsActivity;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.BaseShopListsFragment;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter.ActiveShoppingListsAdapter;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter.ShoppingListsAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public class ActiveListsFragment extends BaseShopListsFragment<ActiveListsContract.Presenter> implements ActiveListsContract.View {

    public static ActiveListsFragment newInstance() {
        return new ActiveListsFragment();
    }

    @BindView(R.id.active_lists_fab)
    View mAddButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_active_lists, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.fetchActiveLists();
    }

    @OnClick(R.id.active_lists_fab)
    public void onAddButtonClicked() {
        mPresenter.onAddButtonClicked();
    }

    @Override
    public void showAddShoppingListDialog() {
        final EditText input = new EditText(getContext());
        input.setHint(R.string.list_name);
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.add_new_list_title)
                .setPositiveButton(R.string.submit, (dialog, which) -> mPresenter.onListCreated(input.getText().toString()))
                .setView(input)
                .create()
                .show();
    }

    @Override
    public void showListDetails(ShoppingList shoppingList) {
        startActivity(ActiveListDetailsActivity.getStartIntent(getContext(), shoppingList));
    }

    @Override
    protected void onItemSwiped(ShoppingList item, int position, boolean isLastItem) {
        mPresenter.archiveItem(item, position, isLastItem);
    }

    @Override
    protected ShoppingListsAdapter createAdapter() {
        return new ActiveShoppingListsAdapter(shoppingList -> mPresenter.onListClicked(shoppingList));
    }

    @Override
    protected void onUndoClicked(ShoppingList shoppingList, int position) {
        mPresenter.unarchiveItem(shoppingList, position);
    }

    @Override
    protected String getUndoMessage(String name) {
        return getResources().getString(R.string.archived_message, name);
    }

    @Override
    public void showErrorView(int errorMessage) {
        super.showErrorView(errorMessage);
        mAddButton.setVisibility(View.GONE);
    }
}