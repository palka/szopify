package com.nekodev.paulina.sadowska.shoppinglist.util.sort;

import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

public class ModificationDateSortingStrategy implements SortingStrategy {

    @Override
    public List<ShoppingListDb> sort(List<ShoppingListDb> unsortedList) {
        List<ShoppingListDb> sortedList = new ArrayList<>(unsortedList);
        Collections.sort(sortedList, new ModificationDateReversedComparator());
        return sortedList;
    }
}
