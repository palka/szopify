package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.active;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListConverter;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingDataSource;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepository;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.sort.SortingStrategy;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public class ActiveListsPresenter implements ActiveListsContract.Presenter {

    private final ActiveListsContract.View mView;
    private final ShoppingDataSource mShoppingRepository;
    private final BaseSchedulerProvider mSchedulerProvider;
    private final CompositeDisposable mDisposable;
    private final ShoppingListConverter mConverter;
    private final SortingStrategy mSortingStrategy;

    @Inject
    ActiveListsPresenter(@NonNull ActiveListsContract.View view,
                         @NonNull ShoppingRepository shoppingRepository,
                         @NonNull BaseSchedulerProvider schedulerProvider,
                         @NonNull ShoppingListConverter converter,
                         @NonNull SortingStrategy mSortingStrategy) {
        this.mView = view;
        this.mShoppingRepository = shoppingRepository;
        this.mSchedulerProvider = schedulerProvider;
        this.mSortingStrategy = mSortingStrategy;
        this.mConverter = converter;
        this.mDisposable = new CompositeDisposable();
    }

    @Inject
    void setupListeners() {
        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
    }

    @Override
    public void onAddButtonClicked() {
        mView.showAddShoppingListDialog();
    }

    @Override
    public void onListCreated(String listName) {
        if (TextUtils.isEmpty(listName)) {
            mView.showError(R.string.error_list_name_empty);
        } else {
            saveNewShoppingList(mConverter.toDbList(listName));
        }
    }

    @Override
    public void archiveItem(ShoppingList shoppingList, int position, boolean isLastItem) {
        mDisposable.add(mShoppingRepository.archiveList(shoppingList.getShoppingListDb())
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        archivedList -> {
                            mView.removeItem(position);
                            mView.showUndoSnackbar(shoppingList, position);
                            if (isLastItem) {
                                mView.showEmptyView();
                            }
                        },
                        e -> mView.showError(R.string.error_archiving)
                ));
    }

    @Override
    public void unarchiveItem(ShoppingList shoppingList, int position) {
        mDisposable.add(mShoppingRepository.unarchiveList(shoppingList.getShoppingListDb())
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        archivedList -> {
                            mView.addShoppingListAtPosition(shoppingList, position);
                            mView.hideEmptyView();
                        },
                        e -> mView.showError(R.string.error_unarchiving)
                ));
    }

    @Override
    public void onListClicked(ShoppingList shoppingList) {
        mView.showListDetails(shoppingList);
    }

    @Override
    public void unSubscribe() {
        mDisposable.clear();
    }

    @Override
    public void fetchActiveLists() {
        mDisposable.add(mShoppingRepository.getActiveLists()
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .map(mSortingStrategy::sort)
                .flatMapIterable(shoppingListDbs -> shoppingListDbs)
                .map(mConverter::toFormattedShoppingList)
                .toList()
                .subscribe(
                        shoppingLists -> {
                            if (shoppingLists.isEmpty()) {
                                mView.showEmptyView();
                            } else {
                                mView.showLists(shoppingLists);
                                mView.hideEmptyView();
                            }
                        },
                        e -> mView.showErrorView(R.string.error_fetching_lists)
                ));
    }

    private void saveNewShoppingList(ShoppingListDb shoppingListDb) {
        mDisposable.add(mShoppingRepository.saveNewShoppingList(shoppingListDb)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        savedShoppingListDb -> {
                            mView.addShoppingListAtPosition(mConverter.toFormattedShoppingList(savedShoppingListDb), 0);
                            mView.hideEmptyView();
                        },
                        e -> mView.showError(R.string.error_creating_list)
                ));
    }
}
