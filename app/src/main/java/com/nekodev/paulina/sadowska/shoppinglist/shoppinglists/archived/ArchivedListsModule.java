package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.archived;

import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.SchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.sort.ModificationDateSortingStrategy;
import com.nekodev.paulina.sadowska.shoppinglist.util.sort.SortingStrategy;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

@Module
class ArchivedListsModule {

    private final ArchivedListsContract.View mView;

    ArchivedListsModule(ArchivedListsContract.View view) {
        this.mView = view;
    }

    @Provides
    BaseSchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }

    @Provides
    ArchivedListsContract.View providesView() {
        return mView;
    }

    @Provides
    SortingStrategy providesSortingStrategy() {
        return new ModificationDateSortingStrategy();
    }
}
