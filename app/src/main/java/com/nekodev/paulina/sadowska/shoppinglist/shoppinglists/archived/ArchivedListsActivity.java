package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.archived;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.ShoppingListApplication;
import com.nekodev.paulina.sadowska.shoppinglist.util.ActionBarUtils;
import com.nekodev.paulina.sadowska.shoppinglist.util.ActivityUtils;

import javax.inject.Inject;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

public class ArchivedListsActivity extends AppCompatActivity {

    @Inject
    ArchivedListsPresenter mPresenter;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, ArchivedListsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        ActionBarUtils.setBackEnabled(getSupportActionBar(), getString(R.string.archived_title));
        ArchivedListsFragment fragment = (ArchivedListsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        if (fragment == null) {
            fragment = ArchivedListsFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.contentFrame);
        }
        DaggerArchivedListsComponent.builder()
                .shoppingRepositoryComponent(((ShoppingListApplication) getApplication()).getRepositoryComponent())
                .archivedListsModule(new ArchivedListsModule(fragment))
                .build()
                .inject(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
