package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials;

import android.support.v7.widget.RecyclerView;

import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public abstract class BaseItemsToBuyAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    protected final List<ItemToBuyDb> itemsToBuy;

    public BaseItemsToBuyAdapter() {
        this.itemsToBuy = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return itemsToBuy.size();
    }

    public void initialize(List<ItemToBuyDb> items) {
        itemsToBuy.clear();
        itemsToBuy.addAll(items);
        notifyDataSetChanged();
    }
}
