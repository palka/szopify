package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.active;

import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.SchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.sort.ModificationDateSortingStrategy;
import com.nekodev.paulina.sadowska.shoppinglist.util.sort.SortingStrategy;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */
@Module
class ActiveListsModule {

    private final ActiveListsContract.View mView;

    ActiveListsModule(ActiveListsContract.View view) {
        this.mView = view;
    }

    @Provides
    BaseSchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }

    @Provides
    ActiveListsContract.View providesView() {
        return mView;
    }

    @Provides
    SortingStrategy providesSortingStrategy() {
        return new ModificationDateSortingStrategy();
    }
}