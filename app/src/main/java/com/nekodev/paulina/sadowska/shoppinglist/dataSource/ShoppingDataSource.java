package com.nekodev.paulina.sadowska.shoppinglist.dataSource;

import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public interface ShoppingDataSource {
    Flowable<List<ShoppingListDb>> getActiveLists();

    Flowable<List<ShoppingListDb>> getArchivedLists();

    Flowable<ShoppingListDb> archiveList(ShoppingListDb shoppingList);

    Flowable<ShoppingListDb> unarchiveList(ShoppingListDb shoppingList);

    Flowable<ShoppingListDb> saveNewShoppingList(ShoppingListDb shoppingList);

    Flowable<List<ItemToBuyDb>> getListItems(int listId);

    Flowable<ItemToBuyDb> addItem(String name, int listId);

    Flowable<ItemToBuyDb> restoreItem(ItemToBuyDb itemToBuyDb);

    Flowable<ItemToBuyDb> editItem(ItemToBuyDb itemToBuyDb);

    Flowable<ItemToBuyDb> removeItem(ItemToBuyDb itemToBuyDb);
}
