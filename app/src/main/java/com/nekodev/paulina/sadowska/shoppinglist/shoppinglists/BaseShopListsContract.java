package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists;

import com.nekodev.paulina.sadowska.shoppinglist.BasePresenter;
import com.nekodev.paulina.sadowska.shoppinglist.BaseView;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;

import java.util.List;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

public interface BaseShopListsContract {

    interface View<T extends BasePresenter> extends BaseView<T> {

        void showLists(List<ShoppingList> shoppingLists);

        void removeItem(int itemId);

        void addShoppingListAtPosition(ShoppingList shoppingList, int position);

        void showUndoSnackbar(ShoppingList shoppingList, int position);

        void showEmptyView();

        void hideEmptyView();
    }
}
