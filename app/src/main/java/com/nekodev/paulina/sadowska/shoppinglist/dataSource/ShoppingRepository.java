package com.nekodev.paulina.sadowska.shoppinglist.dataSource;

import android.support.annotation.VisibleForTesting;

import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */
@Singleton
public class ShoppingRepository implements ShoppingDataSource {

    private final ShoppingDataSource mLocalDataSource;
    @VisibleForTesting
    final List<ShoppingListDb> mActiveListsCache;
    @VisibleForTesting
    final List<ShoppingListDb> mArchivedListsCache;

    @Inject
    ShoppingRepository(ShoppingDataSource localDataSource) {
        this.mLocalDataSource = localDataSource;
        this.mActiveListsCache = new ArrayList<>();
        this.mArchivedListsCache = new ArrayList<>();
    }

    @Override
    public Flowable<List<ShoppingListDb>> getActiveLists() {
        if (!mActiveListsCache.isEmpty()) {
            return Flowable.just(mActiveListsCache);
        }
        return mLocalDataSource.getActiveLists()
                .flatMap(activeShoppingLists -> Flowable.fromIterable(activeShoppingLists)
                        .doOnComplete(() -> {
                            if (!activeShoppingLists.isEmpty()) {
                                mActiveListsCache.addAll(activeShoppingLists);
                            }
                        })
                        .toList().toFlowable());
    }

    @Override
    public Flowable<List<ShoppingListDb>> getArchivedLists() {
        if (!mArchivedListsCache.isEmpty()) {
            return Flowable.just(mArchivedListsCache);
        }
        return mLocalDataSource.getArchivedLists()
                .flatMap(archivedShoppingLists -> Flowable.fromIterable(archivedShoppingLists)
                        .doOnComplete(() -> {
                            if (!archivedShoppingLists.isEmpty()) {
                                mArchivedListsCache.addAll(archivedShoppingLists);
                            }
                        })
                        .toList().toFlowable());
    }

    @Override
    public Flowable<ShoppingListDb> archiveList(ShoppingListDb shoppingList) {
        return mLocalDataSource.archiveList(shoppingList)
                .doOnNext(result -> {
                    removeItemFromCache(mActiveListsCache, shoppingList);
                    if (!mArchivedListsCache.isEmpty()) {
                        mArchivedListsCache.add(shoppingList);
                    }
                });
    }

    @Override
    public Flowable<ShoppingListDb> unarchiveList(ShoppingListDb shoppingList) {
        return mLocalDataSource.unarchiveList(shoppingList)
                .doOnNext(result -> {
                    removeItemFromCache(mArchivedListsCache, shoppingList);
                    if (!mActiveListsCache.isEmpty()) {
                        mActiveListsCache.add(shoppingList);
                    }
                });
    }

    private void removeItemFromCache(List<ShoppingListDb> cache, ShoppingListDb shoppingList) {
        for (int i = 0; i < cache.size(); i++) {
            if (cache.get(i).getId().equals(shoppingList.getId())) {
                cache.remove(i);
            }
        }
    }

    @Override
    public Flowable<ShoppingListDb> saveNewShoppingList(ShoppingListDb shoppingList) {
        return mLocalDataSource.saveNewShoppingList(shoppingList)
                .doOnNext(mActiveListsCache::add);
    }

    @Override
    public Flowable<List<ItemToBuyDb>> getListItems(int listId) {
        return mLocalDataSource.getListItems(listId);
    }

    @Override
    public Flowable<ItemToBuyDb> addItem(String name, int listId) {
        return mLocalDataSource.addItem(name, listId);
    }

    @Override
    public Flowable<ItemToBuyDb> restoreItem(ItemToBuyDb itemToBuyDb) {
        return mLocalDataSource.restoreItem(itemToBuyDb);
    }

    @Override
    public Flowable<ItemToBuyDb> editItem(ItemToBuyDb itemToBuyDb) {
        return mLocalDataSource.editItem(itemToBuyDb);
    }

    @Override
    public Flowable<ItemToBuyDb> removeItem(ItemToBuyDb itemToBuyDb) {
        return mLocalDataSource.removeItem(itemToBuyDb);
    }
}
