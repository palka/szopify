package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.nekodev.paulina.sadowska.shoppinglist.BaseFragment;
import com.nekodev.paulina.sadowska.shoppinglist.BasePresenter;
import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter.ShoppingListsAdapter;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter.ShoppingListsItemTouchHelper;
import com.nekodev.paulina.sadowska.shoppinglist.view.ErrorViewHolder;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

public abstract class BaseShopListsFragment<T extends BasePresenter> extends BaseFragment<T> implements BaseShopListsContract.View<T> {
    private static final String BUNDLE_RECYCLER_LAYOUT = "BaseShopListsFragment.recycler.layout";

    @BindView(R.id.recycler_view)
    protected RecyclerView mRecyclerView;

    protected ShoppingListsAdapter mAdapter;
    private ErrorViewHolder mEmptyView;

    @SuppressWarnings("deprecation")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        configureRecyclerView();
        setRecyclerViewAdapter();
        if (savedInstanceState != null) {
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
            mRecyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
        }
        addRecyclerViewSwipeToDelete();
        prepareEmptyView(view);
        super.onViewCreated(view, savedInstanceState);
    }

    private void prepareEmptyView(View view) {
        mEmptyView = new ErrorViewHolder(view)
                .setImageResource(R.drawable.szopify_logo)
                .setMessage(R.string.message_list_empty);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, mRecyclerView.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void showLists(List<ShoppingList> shoppingLists) {
        mAdapter.initialize(shoppingLists);
    }

    @Override
    public void removeItem(int itemId) {
        mAdapter.removeItem(itemId);
    }

    @Override
    public void addShoppingListAtPosition(ShoppingList shoppingList, int position) {
        mAdapter.addItem(shoppingList, position);
        mRecyclerView.smoothScrollToPosition(position);
    }

    @Override
    public void showUndoSnackbar(ShoppingList shoppingList, int position) {
        Snackbar
                .make(mRecyclerView, getUndoMessage(shoppingList.getName()), Snackbar.LENGTH_LONG)
                .setAction(R.string.undo, view -> onUndoClicked(shoppingList, position))
                .setActionTextColor(ContextCompat.getColor(getContext(), R.color.amber_500))
                .show();
    }

    @Override
    public void showErrorView(int errorMessage) {
        new ErrorViewHolder(getView())
                .setImageResource(R.drawable.szopify_error)
                .setMessage(errorMessage)
                .show();
    }

    @Override
    public void showEmptyView() {
        mEmptyView.show();
    }

    @Override
    public void hideEmptyView() {
        mEmptyView.hide();
    }

    protected abstract void onUndoClicked(ShoppingList shoppingList, int position);

    protected abstract String getUndoMessage(String name);

    protected abstract void onItemSwiped(ShoppingList item, int position, boolean isLastItem);

    private void configureRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setRecyclerViewAdapter() {
        if (mRecyclerView.getAdapter() == null) {
            mAdapter = createAdapter();
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter = (ShoppingListsAdapter) mRecyclerView.getAdapter();
        }
    }

    protected abstract ShoppingListsAdapter createAdapter();

    private void addRecyclerViewSwipeToDelete() {
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback =
                new ShoppingListsItemTouchHelper(
                        0,
                        ItemTouchHelper.LEFT,
                        (viewHolder, direction, position) ->
                                onItemSwiped(
                                        mAdapter.getItem(position),
                                        position,
                                        mAdapter.getItemCount() <= 1));
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);
    }
}
