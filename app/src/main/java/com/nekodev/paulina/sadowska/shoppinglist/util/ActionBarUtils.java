package com.nekodev.paulina.sadowska.shoppinglist.util;


import android.support.v7.app.ActionBar;

import javax.annotation.Nullable;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */

public class ActionBarUtils {

    public static void setBackEnabled(@Nullable ActionBar actionBar, String screenTitle) {
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(screenTitle);
        }
    }
}
