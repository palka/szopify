package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nekodev.paulina.sadowska.shoppinglist.BaseFragment;
import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived.adapter.ArchivedItemsToBuyAdapter;
import com.nekodev.paulina.sadowska.shoppinglist.view.ErrorViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ArchivedListDetailsFragment extends BaseFragment<ArchivedListDetailsContract.Presenter> implements ArchivedListDetailsContract.View {
    private static final String BUNDLE_RECYCLER_LAYOUT = "ArchivedListDetailsFragment.recycler.layout";

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    protected ArchivedItemsToBuyAdapter mAdapter;

    public static ArchivedListDetailsFragment newInstance() {
        return new ArchivedListDetailsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basic, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        configureRecyclerView();
        setRecyclerViewAdapter();
        if (savedInstanceState != null) {
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
            mRecyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, mRecyclerView.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void showItems(List<ItemToBuyDb> itemsToBuy) {
        mAdapter.initialize(itemsToBuy);
    }

    private void configureRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    private void setRecyclerViewAdapter() {
        if (mRecyclerView.getAdapter() == null) {
            mAdapter = new ArchivedItemsToBuyAdapter();
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter = (ArchivedItemsToBuyAdapter) mRecyclerView.getAdapter();
        }
    }

    @Override
    public void showErrorView(int errorMessage) {
        new ErrorViewHolder(getView())
                .setImageResource(R.drawable.szopify_error)
                .setMessage(errorMessage)
                .show();
    }
}
