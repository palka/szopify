package com.nekodev.paulina.sadowska.shoppinglist.data;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ItemToBuyDb extends RealmObject implements Parcelable {

    @PrimaryKey
    private int id;

    @Required
    private String name;

    private int listId;

    private boolean isChecked;

    public ItemToBuyDb() {

    }

    public ItemToBuyDb(int id, String name, int listId, boolean isChecked) {
        this.id = id;
        this.name = name;
        this.listId = listId;
        this.isChecked = isChecked;
    }

    public ItemToBuyDb(String name, int listId) {
        this.name = name;
        this.listId = listId;
    }

    ItemToBuyDb(Parcel in) {
        id = in.readInt();
        name = in.readString();
        listId = in.readInt();
        isChecked = in.readByte() != 0;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getListId() {
        return listId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(listId);
        dest.writeByte((byte) (isChecked ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemToBuyDb> CREATOR = new Creator<ItemToBuyDb>() {
        @Override
        public ItemToBuyDb createFromParcel(Parcel in) {
            return new ItemToBuyDb(in);
        }

        @Override
        public ItemToBuyDb[] newArray(int size) {
            return new ItemToBuyDb[size];
        }
    };

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
