package com.nekodev.paulina.sadowska.shoppinglist.data;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public class ShoppingListDb extends RealmObject implements Parcelable {

    @PrimaryKey
    private int id;

    @Required
    private String name;

    private long modificationTimestamp;
    private boolean isArchived;

    public ShoppingListDb() {
    }

    public ShoppingListDb(Integer id, String name, Long modificationTimestamp, boolean isArchived) {
        this(name, modificationTimestamp, isArchived);
        this.id = id;
    }

    ShoppingListDb(String name, Long modificationTimestamp, boolean isArchived) {
        this.name = name;
        this.modificationTimestamp = modificationTimestamp;
        this.isArchived = isArchived;
    }

    protected ShoppingListDb(Parcel in) {
        id = in.readInt();
        name = in.readString();
        modificationTimestamp = in.readLong();
        isArchived = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeLong(modificationTimestamp);
        dest.writeByte((byte) (isArchived ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShoppingListDb> CREATOR = new Creator<ShoppingListDb>() {
        @Override
        public ShoppingListDb createFromParcel(Parcel in) {
            return new ShoppingListDb(in);
        }

        @Override
        public ShoppingListDb[] newArray(int size) {
            return new ShoppingListDb[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public Long getModificationTimestamp() {
        return modificationTimestamp;
    }

    public void setId(int id) {
        this.id = id;
    }
}
