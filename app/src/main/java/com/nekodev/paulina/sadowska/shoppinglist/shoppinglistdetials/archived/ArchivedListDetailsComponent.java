package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived;

import com.nekodev.paulina.sadowska.shoppinglist.ApplicationModule;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepositoryComponent;
import com.nekodev.paulina.sadowska.shoppinglist.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

@FragmentScope
@Component(dependencies = ShoppingRepositoryComponent.class, modules = {ArchivedListDetailsModule.class, ApplicationModule.class})
public interface ArchivedListDetailsComponent {
    void inject(ArchivedListDetailsActivity detailsActivity);
}
