package com.nekodev.paulina.sadowska.shoppinglist.view;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.shoppinglist.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */

public class ErrorViewHolder {

    @BindView(R.id.error_layout)
    View mLayout;
    @BindView(R.id.error_image)
    ImageView mImage;
    @BindView(R.id.error_text)
    TextView mText;

    public ErrorViewHolder(View view) {
        ButterKnife.bind(this, view);
    }

    public ErrorViewHolder setImageResource(@DrawableRes int imageResource) {
        mImage.setImageResource(imageResource);
        return this;
    }

    public ErrorViewHolder setMessage(@StringRes int message) {
        mText.setText(message);
        return this;
    }

    public void show() {
        mLayout.setVisibility(View.VISIBLE);
    }

    public void hide() {
        mLayout.setVisibility(View.INVISIBLE);
    }
}
