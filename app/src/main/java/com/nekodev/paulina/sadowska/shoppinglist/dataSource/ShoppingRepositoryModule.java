package com.nekodev.paulina.sadowska.shoppinglist.dataSource;

import com.nekodev.paulina.sadowska.shoppinglist.dataSource.local.ShoppingLocalDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */
@Module
class ShoppingRepositoryModule {


    @Provides
    @Singleton
    ShoppingDataSource provideSHoppingLocalDataSource() {
        return new ShoppingLocalDataSource();
    }
}
