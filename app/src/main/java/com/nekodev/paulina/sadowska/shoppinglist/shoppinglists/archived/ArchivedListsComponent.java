package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.archived;

import com.nekodev.paulina.sadowska.shoppinglist.ApplicationModule;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepositoryComponent;
import com.nekodev.paulina.sadowska.shoppinglist.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

@FragmentScope
@Component(dependencies = ShoppingRepositoryComponent.class, modules = {ArchivedListsModule.class, ApplicationModule.class})
public interface ArchivedListsComponent {
    void inject(ArchivedListsActivity archivedListsActivity);
}
