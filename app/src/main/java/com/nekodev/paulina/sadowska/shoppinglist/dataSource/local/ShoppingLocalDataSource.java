package com.nekodev.paulina.sadowska.shoppinglist.dataSource.local;

import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingDataSource;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public class ShoppingLocalDataSource implements ShoppingDataSource {
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_IS_ARCHIVED = "isArchived";
    private static final String COLUMN_LIST_ID = "listId";

    @Override
    public Flowable<List<ShoppingListDb>> getActiveLists() {
        return getLists(false);
    }

    @Override
    public Flowable<List<ShoppingListDb>> getArchivedLists() {
        return getLists(true);
    }

    @Override
    public Flowable<ShoppingListDb> archiveList(ShoppingListDb shoppingList) {
        shoppingList.setArchived(true);
        return insertOrUpdate(shoppingList);
    }

    @Override
    public Flowable<ShoppingListDb> unarchiveList(ShoppingListDb shoppingList) {
        shoppingList.setArchived(false);
        return insertOrUpdate(shoppingList);
    }

    private Flowable<ShoppingListDb> insertOrUpdate(ShoppingListDb shoppingList) {
        try (Realm realm = getRealm()) {
            realm.executeTransaction(
                    (realm1) -> realm1.insertOrUpdate(shoppingList));
            return Flowable.just(shoppingList);
        }
    }

    @Override
    public Flowable<ShoppingListDb> saveNewShoppingList(ShoppingListDb shoppingList) {
        shoppingList.setId(getNextListId());
        try (Realm realm = getRealm()) {
            realm.executeTransaction(
                    (realm1) -> realm1.copyToRealm(shoppingList));
            return Flowable.just(shoppingList);
        }
    }

    @Override
    public Flowable<List<ItemToBuyDb>> getListItems(int listId) {
        return Flowable.fromCallable(() -> {
            try (Realm realm = Realm.getDefaultInstance()) {
                RealmResults<ItemToBuyDb> realmResults = realm
                        .where(ItemToBuyDb.class)
                        .equalTo(COLUMN_LIST_ID, listId)
                        .findAll();
                List<ItemToBuyDb> result = new ArrayList<>();
                for (ItemToBuyDb item : realmResults) {
                    result.add(new ItemToBuyDb(item.getId(), item.getName(), item.getListId(), item.isChecked()));
                }
                return result;
            }
        });
    }

    @Override
    public Flowable<ItemToBuyDb> addItem(String name, int listId) {
        ItemToBuyDb itemToBuyDb = new ItemToBuyDb(name, listId);
        itemToBuyDb.setId(getNextListItemId());
        return restoreItem(itemToBuyDb);
    }

    @Override
    public Flowable<ItemToBuyDb> restoreItem(ItemToBuyDb itemToBuyDb) {
        try (Realm realm = getRealm()) {
            realm.executeTransaction(
                    (realm1) -> realm1.copyToRealm(itemToBuyDb));
            return Flowable.just(itemToBuyDb);
        }
    }

    @Override
    public Flowable<ItemToBuyDb> editItem(ItemToBuyDb itemToBuyDb) {
        try (Realm realm = getRealm()) {
            realm.executeTransaction(
                    (realm1) -> realm1.copyToRealmOrUpdate(itemToBuyDb));
            return Flowable.just(itemToBuyDb);
        }
    }

    @Override
    public Flowable<ItemToBuyDb> removeItem(ItemToBuyDb itemToBuyDb) {
        return Flowable.fromCallable(() -> {
            try (Realm realm = Realm.getDefaultInstance()) {
                realm.executeTransaction(
                        (realm1) -> {
                            RealmResults<ItemToBuyDb> realmResults = realm1
                                    .where(ItemToBuyDb.class)
                                    .equalTo(COLUMN_ID, itemToBuyDb.getId())
                                    .findAll();
                            realmResults.deleteAllFromRealm();
                        });
                return itemToBuyDb;
            }
        });
    }

    private Flowable<List<ShoppingListDb>> getLists(boolean archived) {
        return Flowable.fromCallable(() -> {
            try (Realm realm = Realm.getDefaultInstance()) {
                RealmResults<ShoppingListDb> realmResults = realm
                        .where(ShoppingListDb.class)
                        .equalTo(COLUMN_IS_ARCHIVED, archived)
                        .findAll();
                List<ShoppingListDb> result = new ArrayList<>();
                for (ShoppingListDb realmResult : realmResults) {
                    result.add(new ShoppingListDb(realmResult.getId(), realmResult.getName(), realmResult.getModificationTimestamp(), archived));
                }
                return result;
            }
        });
    }

    private int getNextListId() {
        try (Realm realm = Realm.getDefaultInstance()) {
            Number number = realm.where(ShoppingListDb.class).max(COLUMN_ID);
            if (number != null) {
                return number.intValue() + 1;
            } else {
                return 0;
            }
        }
    }


    private int getNextListItemId() {
        try (Realm realm = Realm.getDefaultInstance()) {
            Number number = realm.where(ItemToBuyDb.class).max(COLUMN_ID);
            if (number != null) {
                return number.intValue() + 1;
            } else {
                return 0;
            }
        }
    }

    private Realm getRealm() {
        return Realm.getDefaultInstance();
    }
}
