package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.ShoppingListApplication;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.util.ActionBarUtils;
import com.nekodev.paulina.sadowska.shoppinglist.util.ActivityUtils;

import javax.inject.Inject;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ArchivedListDetailsActivity extends AppCompatActivity {

    private static final String EXTRA_SHOPPING_LIST = "shoppingListArg";

    @Inject
    ArchivedListDetailsPresenter mPresenter;

    public static Intent getStartIntent(Context context, ShoppingList shoppingList) {
        return new Intent(context, ArchivedListDetailsActivity.class)
                .putExtra(EXTRA_SHOPPING_LIST, shoppingList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        ShoppingList shoppingList = getIntent().getParcelableExtra(EXTRA_SHOPPING_LIST);
        ActionBarUtils.setBackEnabled(getSupportActionBar(), shoppingList.getName());
        ArchivedListDetailsFragment fragment = (ArchivedListDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        if (fragment == null) {
            fragment = ArchivedListDetailsFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.contentFrame);
        }
        DaggerArchivedListDetailsComponent.builder()
                .shoppingRepositoryComponent(((ShoppingListApplication) getApplication()).getRepositoryComponent())
                .archivedListDetailsModule(new ArchivedListDetailsModule(fragment, shoppingList))
                .build()
                .inject(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
