package com.nekodev.paulina.sadowska.shoppinglist.util.sort;

import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;

import java.util.List;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

public interface SortingStrategy {
    List<ShoppingListDb> sort(List<ShoppingListDb> unsortedList);
}
