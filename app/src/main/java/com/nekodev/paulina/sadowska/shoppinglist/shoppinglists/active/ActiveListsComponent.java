package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.active;

import com.nekodev.paulina.sadowska.shoppinglist.ApplicationModule;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepositoryComponent;
import com.nekodev.paulina.sadowska.shoppinglist.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

@FragmentScope
@Component(dependencies = ShoppingRepositoryComponent.class, modules = {ActiveListsModule.class, ApplicationModule.class})
public interface ActiveListsComponent {
    void inject(ActiveListsActivity activeListsActivity);
}