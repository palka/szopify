package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.nekodev.paulina.sadowska.shoppinglist.BaseFragment;
import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active.adapter.ActiveItemsToBuyAdapter;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active.adapter.ActiveListDetailsItemTouchHelper;
import com.nekodev.paulina.sadowska.shoppinglist.view.ErrorViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ActiveListDetailsFragment extends BaseFragment<ActiveListDetailsContract.Presenter> implements ActiveListDetailsContract.View {

    private static final String BUNDLE_RECYCLER_LAYOUT = "ActiveListDetailsFragment.recycler.layout";

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    protected ActiveItemsToBuyAdapter mAdapter;

    public static ActiveListDetailsFragment newInstance() {
        return new ActiveListDetailsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basic, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        configureRecyclerView();
        setRecyclerViewAdapter();
        if (savedInstanceState != null) {
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
            mRecyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
        }
        addRecyclerViewSwipeToEdit();
        initializeAddNewTaskListener();
        super.onViewCreated(view, savedInstanceState);
    }

    private void initializeAddNewTaskListener() {
        ((ActiveListDetailsActivity) getActivity()).setOnAddItemListener(new ActiveListDetailsActivity.OnAddItemListener() {
            @Override
            public void onAddClicked(EditText taskNameEdit) {
                if (taskNameEdit != null) {
                    mPresenter.addItemToBuy(taskNameEdit.getText().toString());
                    taskNameEdit.setText("");
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, mRecyclerView.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void showItems(List<ItemToBuyDb> itemsToBuy) {
        mAdapter.initialize(itemsToBuy);
    }

    @Override
    public void addItemAtPosition(ItemToBuyDb itemToBuyDb, int position) {
        mAdapter.addItem(itemToBuyDb, position);
        mRecyclerView.smoothScrollToPosition(position);
    }

    @Override
    public void addItemOnEnd(ItemToBuyDb itemToBuy) {
        int position = mAdapter.getItemCount();
        mAdapter.addItem(itemToBuy, position);
        mRecyclerView.smoothScrollToPosition(position);
    }

    @Override
    public void removeItem(int position) {
        mAdapter.removeItem(position);
    }

    @Override
    public void showUndoSnackbar(ItemToBuyDb itemToBuy, int position) {
        Snackbar
                .make(mRecyclerView, getUndoMessage(itemToBuy.getName()), Snackbar.LENGTH_LONG)
                .setAction(R.string.undo, view -> mPresenter.onUndoClicked(itemToBuy, position))
                .setActionTextColor(ContextCompat.getColor(getContext(), R.color.amber_500))
                .show();
    }

    @Override
    public void showEditDialog(ItemToBuyDb itemToBuy, int position) {
        final EditText input = new EditText(getContext());
        input.setHint(R.string.list_name);
        input.setText(itemToBuy.getName());
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.add_new_list_title)
                .setPositiveButton(R.string.submit, (dialog, which) -> mPresenter.onNameChanged(input.getText().toString(), itemToBuy, position))
                .setView(input)
                .create()
                .show();
    }

    @Override
    public void refreshItem(ItemToBuyDb editedItem, int position) {
        mAdapter.replaceItem(editedItem, position);
    }

    private String getUndoMessage(String name) {
        return getResources().getString(R.string.removed_message, name);
    }

    private void configureRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setRecyclerViewAdapter() {
        if (mRecyclerView.getAdapter() == null) {
            mAdapter = createAdapter();
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter = (ActiveItemsToBuyAdapter) mRecyclerView.getAdapter();
        }
    }

    protected ActiveItemsToBuyAdapter createAdapter() {
        ActiveItemsToBuyAdapter adapter = new ActiveItemsToBuyAdapter();
        adapter.setOnEditListener((itemToBuyDb, position) -> mPresenter.onEditClicked(itemToBuyDb, position));
        adapter.setOnCheckChangedListener((isChecked, position) -> {
            ItemToBuyDb itemToBuy = adapter.getItem(position);
            mPresenter.onCheckChanged(itemToBuy, isChecked);
        });
        return adapter;
    }

    private void addRecyclerViewSwipeToEdit() {
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback =
                new ActiveListDetailsItemTouchHelper(
                        0,
                        ItemTouchHelper.LEFT,
                        (viewHolder, direction, position) -> onItemSwiped(mAdapter.getItem(position), position));
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);
    }

    private void onItemSwiped(ItemToBuyDb item, int position) {
        mPresenter.deleteItem(item, position);
    }

    @Override
    public void showErrorView(int errorMessage) {
        new ErrorViewHolder(getView())
                .setImageResource(R.drawable.szopify_error)
                .setMessage(errorMessage)
                .show();
    }
}
