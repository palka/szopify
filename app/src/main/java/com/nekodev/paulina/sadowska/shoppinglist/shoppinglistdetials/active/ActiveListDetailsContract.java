package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active;

import com.nekodev.paulina.sadowska.shoppinglist.BasePresenter;
import com.nekodev.paulina.sadowska.shoppinglist.BaseView;
import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;

import java.util.List;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public interface ActiveListDetailsContract {
    interface View extends BaseView<Presenter> {
        void showItems(List<ItemToBuyDb> itemsToBuy);

        void addItemAtPosition(ItemToBuyDb itemToBuyDb, int position);

        void addItemOnEnd(ItemToBuyDb item);

        void removeItem(int position);

        void showUndoSnackbar(ItemToBuyDb itemToBuy, int position);

        void showEditDialog(ItemToBuyDb itemToBuy, int position);

        void refreshItem(ItemToBuyDb editedItem, int position);
    }

    interface Presenter extends BasePresenter {

        void addItemToBuy(String name);

        void deleteItem(ItemToBuyDb item, int position);

        void onUndoClicked(ItemToBuyDb itemToBuy, int position);

        void onEditClicked(ItemToBuyDb itemToBuyDb, int position);

        void onCheckChanged(ItemToBuyDb itemToBuy, boolean isChecked);

        void onNameChanged(String newName, ItemToBuyDb itemToBuy, int position);
    }
}
