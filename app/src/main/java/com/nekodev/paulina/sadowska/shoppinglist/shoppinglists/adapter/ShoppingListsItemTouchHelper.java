package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.nekodev.paulina.sadowska.shoppinglist.util.RecyclerItemTouchHelper;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

public class ShoppingListsItemTouchHelper extends RecyclerItemTouchHelper {

    public ShoppingListsItemTouchHelper(int dragDirections, int swipeDirections, RecyclerItemTouchHelperListener listener) {
        super(dragDirections, swipeDirections, listener);
    }

    @Override
    protected View getForeground(RecyclerView.ViewHolder viewHolder) {
        return ((ShoppingListsAdapter.ShoppingListViewHolder) viewHolder).foregroundLayout;
    }
}
