package com.nekodev.paulina.sadowska.shoppinglist.dataSource;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

@Singleton
@Component(modules = {ShoppingRepositoryModule.class})
public interface ShoppingRepositoryComponent {
    ShoppingRepository getShoppingRepository();
}
