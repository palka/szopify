package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public abstract class ShoppingListsAdapter extends RecyclerView.Adapter<ShoppingListsAdapter.ShoppingListViewHolder> {

    public static final int LAYOUT_TYPE_ACTIVE = 0;
    public static final int LAYOUT_TYPE_ARCHIVED = 1;

    private final List<ShoppingList> mShoppingLists;
    private OnItemClickedListener mListener;


    public ShoppingListsAdapter(OnItemClickedListener onItemClickedListener) {
        this.mListener = onItemClickedListener;
        this.mShoppingLists = new ArrayList<>();
    }

    @Override
    public ShoppingListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(getLayoutResource(), parent, false);
        return new ShoppingListViewHolder(view);
    }

    @LayoutRes
    protected abstract int getLayoutResource();

    @Override
    public void onBindViewHolder(ShoppingListViewHolder holder, int position) {
        holder.bind(mShoppingLists.get(position));
    }

    @Override
    public int getItemCount() {
        return mShoppingLists.size();
    }

    public void initialize(List<ShoppingList> items) {
        mShoppingLists.clear();
        mShoppingLists.addAll(items);
        notifyDataSetChanged();
    }

    public void addItem(ShoppingList shoppingList, int position) {
        mShoppingLists.add(position, shoppingList);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mShoppingLists.remove(position);
        notifyItemRemoved(position);
    }

    public ShoppingList getItem(int position) {
        return mShoppingLists.get(position);
    }

    class ShoppingListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.shopping_list_foreground)
        View foregroundLayout;
        @BindView(R.id.shopping_list_name)
        TextView mName;
        @BindView(R.id.shopping_list_modification_date)
        TextView mModificationDate;

        ShoppingListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(ShoppingList shoppingList) {
            this.mName.setText(shoppingList.getName());
            this.mModificationDate.setText(shoppingList.getModificationDateFormatted());
            if (mListener != null) {
                foregroundLayout.setOnClickListener(view -> mListener.onItemClicked(shoppingList));
            }
        }
    }

    public interface OnItemClickedListener {
        void onItemClicked(ShoppingList shoppingList);
    }
}
