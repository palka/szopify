package com.nekodev.paulina.sadowska.shoppinglist;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

import com.nekodev.paulina.sadowska.shoppinglist.dataSource.DaggerShoppingRepositoryComponent;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepositoryComponent;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public class ShoppingListApplication extends Application {

    private ShoppingRepositoryComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);
        mComponent = DaggerShoppingRepositoryComponent.builder()
                .build();
    }

    public ShoppingRepositoryComponent getRepositoryComponent() {
        return mComponent;
    }

    @VisibleForTesting
    public void setAppComponent(ShoppingRepositoryComponent component) {
        mComponent = component;
    }
}
