package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.active;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.ShoppingListApplication;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.archived.ArchivedListsActivity;
import com.nekodev.paulina.sadowska.shoppinglist.util.ActivityUtils;

import javax.inject.Inject;

public class ActiveListsActivity extends AppCompatActivity {

    @Inject
    ActiveListsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        ActiveListsFragment fragment = (ActiveListsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        if (fragment == null) {
            fragment = ActiveListsFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.contentFrame);
        }
        DaggerActiveListsComponent.builder()
                .shoppingRepositoryComponent(((ShoppingListApplication) getApplication()).getRepositoryComponent())
                .activeListsModule(new ActiveListsModule(fragment))
                .build()
                .inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.archive_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_see_archived:
                startActivity(ArchivedListsActivity.getStartIntent(this));
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
