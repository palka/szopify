package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.active;

import com.nekodev.paulina.sadowska.shoppinglist.BasePresenter;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.BaseShopListsContract;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public interface ActiveListsContract {
    interface View extends BaseShopListsContract.View<Presenter> {

        void showAddShoppingListDialog();

        void addShoppingListAtPosition(ShoppingList shoppingList, int position);

        void showListDetails(ShoppingList shoppingList);
    }

    interface Presenter extends BasePresenter {
        void onAddButtonClicked();

        void onListCreated(String name);

        void archiveItem(ShoppingList item, int position, boolean isLastItem);

        void unarchiveItem(ShoppingList shoppingList, int position);

        void onListClicked(ShoppingList shoppingList);

        void fetchActiveLists();
    }
}
