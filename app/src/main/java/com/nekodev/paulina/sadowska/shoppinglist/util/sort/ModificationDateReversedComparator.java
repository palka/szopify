package com.nekodev.paulina.sadowska.shoppinglist.util.sort;

import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;

import java.util.Comparator;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

class ModificationDateReversedComparator implements Comparator<ShoppingListDb> {

    @Override
    public int compare(ShoppingListDb o1, ShoppingListDb o2) {
        return (-1) * o1.getModificationTimestamp().compareTo(o2.getModificationTimestamp());
    }
}
