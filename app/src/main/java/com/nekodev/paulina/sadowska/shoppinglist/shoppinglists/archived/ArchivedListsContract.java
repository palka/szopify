package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.archived;

import com.nekodev.paulina.sadowska.shoppinglist.BasePresenter;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.BaseShopListsContract;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

public interface ArchivedListsContract {
    interface View extends BaseShopListsContract.View<ArchivedListsContract.Presenter> {
        void showListDetails(ShoppingList shoppingList);
    }

    interface Presenter extends BasePresenter {
        void archiveItem(ShoppingList item, int position);

        void unarchiveItem(ShoppingList shoppingList, int position, boolean isLastItem);

        void onListClicked(ShoppingList shoppingList);
    }
}
