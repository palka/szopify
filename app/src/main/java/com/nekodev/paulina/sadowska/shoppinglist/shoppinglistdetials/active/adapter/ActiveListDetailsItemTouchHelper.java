package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.nekodev.paulina.sadowska.shoppinglist.util.RecyclerItemTouchHelper;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ActiveListDetailsItemTouchHelper extends RecyclerItemTouchHelper {

    public ActiveListDetailsItemTouchHelper(int dragDirections, int swipeDirections, RecyclerItemTouchHelperListener listener) {
        super(dragDirections, swipeDirections, listener);
    }

    @Override
    protected View getForeground(RecyclerView.ViewHolder viewHolder) {
        return ((ActiveItemsToBuyAdapter.ViewHolder) viewHolder).foregroundLayout;
    }
}
