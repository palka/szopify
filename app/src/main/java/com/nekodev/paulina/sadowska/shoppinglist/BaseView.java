package com.nekodev.paulina.sadowska.shoppinglist;

import android.support.annotation.StringRes;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);

    void showError(@StringRes int errorMessage);

    void showErrorView(@StringRes int errorMessage);
}
