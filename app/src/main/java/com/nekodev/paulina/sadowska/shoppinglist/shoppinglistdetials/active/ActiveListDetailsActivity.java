package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.active;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.ShoppingListApplication;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.util.ActivityUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ActiveListDetailsActivity extends AppCompatActivity {

    private static final String EXTRA_SHOPPING_LIST = "shoppingListArg";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.details_edit)
    EditText mNewTaskNameEdit;
    @BindView(R.id.details_button)
    View mAddItemButton;

    @Inject
    ActiveListDetailsPresenter mPresenter;

    public static Intent getStartIntent(Context context, ShoppingList shoppingList) {
        return new Intent(context, ActiveListDetailsActivity.class)
                .putExtra(EXTRA_SHOPPING_LIST, shoppingList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_list_details);
        ButterKnife.bind(this);
        ShoppingList shoppingList = getIntent().getParcelableExtra(EXTRA_SHOPPING_LIST);
        initializeActionBar(shoppingList.getName());
        ActiveListDetailsFragment fragment = (ActiveListDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        if (fragment == null) {
            fragment = ActiveListDetailsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.contentFrame);
        }
        DaggerActiveListDetailsComponent.builder()
                .shoppingRepositoryComponent(((ShoppingListApplication) getApplication()).getRepositoryComponent())
                .activeListDetailsModule(new ActiveListDetailsModule(fragment, shoppingList))
                .build()
                .inject(this);
    }

    @SuppressLint("RestrictedApi")
    private void initializeActionBar(String title) {
        mToolbar.setCollapsible(true);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void setOnAddItemListener(OnAddItemListener listener) {
        mAddItemButton.setOnClickListener(v -> listener.onAddClicked(mNewTaskNameEdit));
        mNewTaskNameEdit.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                listener.onAddClicked(mNewTaskNameEdit);
                return true;
            }
            return false;
        });
    }

    public interface OnAddItemListener {
        void onAddClicked(EditText taskNameEdit);
    }
}
