package com.nekodev.paulina.sadowska.shoppinglist.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Paulina Sadowska on 18.10.2017.
 */

public class ShoppingListConverter {

    private final SimpleDateFormat dateFormat;

    public ShoppingListConverter(SimpleDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    public ShoppingListDb toDbList(String listName) {
        return new ShoppingListDb(
                listName,
                System.currentTimeMillis(),
                false);
    }

    public ShoppingList toFormattedShoppingList(ShoppingListDb shoppingListDb) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(shoppingListDb.getModificationTimestamp());
        String dateFormatted = dateFormat.format(calendar.getTime());
        return new ShoppingList(shoppingListDb, dateFormatted);
    }
}
