package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived;

import android.support.annotation.NonNull;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingDataSource;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepository;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Paulina Sadowska on 19.10.2017.
 */

public class ArchivedListDetailsPresenter implements ArchivedListDetailsContract.Presenter {

    private final ArchivedListDetailsContract.View mView;
    private final ShoppingDataSource mShoppingRepository;
    private final BaseSchedulerProvider mSchedulerProvider;
    private final CompositeDisposable mDisposable;
    private final ShoppingList mShoppingList;

    @Inject
    ArchivedListDetailsPresenter(@NonNull ArchivedListDetailsContract.View view,
                                 @NonNull ShoppingRepository shoppingRepository,
                                 @NonNull BaseSchedulerProvider schedulerProvider,
                                 @NonNull ShoppingList shoppingList) {
        this.mView = view;
        this.mShoppingRepository = shoppingRepository;
        this.mSchedulerProvider = schedulerProvider;
        this.mShoppingList = shoppingList;
        this.mDisposable = new CompositeDisposable();
    }

    @Inject
    void setupListeners() {
        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        fetchListDetails();
    }

    @Override
    public void unSubscribe() {
        mDisposable.dispose();
    }

    private void fetchListDetails() {
        mDisposable.add(mShoppingRepository.getListItems(mShoppingList.getId())
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        mView::showItems,
                        e -> mView.showErrorView(R.string.error_fetching_list_details)
                ));
    }
}
