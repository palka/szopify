package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.adapter;

import com.nekodev.paulina.sadowska.shoppinglist.R;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */

public class ActiveShoppingListsAdapter extends ShoppingListsAdapter {

    public ActiveShoppingListsAdapter(OnItemClickedListener onItemClickedListener) {
        super(onItemClickedListener);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.partial_active_list_item;
    }
}
