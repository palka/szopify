package com.nekodev.paulina.sadowska.shoppinglist.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.VisibleForTesting;

/**
 * Created by Paulina Sadowska on 17.10.2017.
 */

public class ShoppingList implements Parcelable {

    private String modificationDateFormatted;
    private ShoppingListDb shoppingListDb;

    ShoppingList(ShoppingListDb shoppingListDb, String modificationDateFormatted) {
        this.shoppingListDb = shoppingListDb;
        this.modificationDateFormatted = modificationDateFormatted;
    }

    protected ShoppingList(Parcel in) {
        modificationDateFormatted = in.readString();
        shoppingListDb = in.readParcelable(ShoppingListDb.class.getClassLoader());
    }

    @VisibleForTesting
    public ShoppingList() {
        this.shoppingListDb = new ShoppingListDb();
        this.modificationDateFormatted = "";
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(modificationDateFormatted);
        dest.writeParcelable(shoppingListDb, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShoppingList> CREATOR = new Creator<ShoppingList>() {
        @Override
        public ShoppingList createFromParcel(Parcel in) {
            return new ShoppingList(in);
        }

        @Override
        public ShoppingList[] newArray(int size) {
            return new ShoppingList[size];
        }
    };

    public String getName() {
        return shoppingListDb.getName();
    }

    public int getId() {
        return shoppingListDb.getId();
    }

    public String getModificationDateFormatted() {
        return modificationDateFormatted;
    }

    public ShoppingListDb getShoppingListDb() {
        return shoppingListDb;
    }
}
