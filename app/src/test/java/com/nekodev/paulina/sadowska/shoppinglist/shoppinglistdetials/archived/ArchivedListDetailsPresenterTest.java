package com.nekodev.paulina.sadowska.shoppinglist.shoppinglistdetials.archived;

import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ItemToBuyDb;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepository;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.ImmediateSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */
@RunWith(RobolectricTestRunner.class)
public class ArchivedListDetailsPresenterTest {

    private static final Integer SHOPPING_LIST_ID = 669;

    private ShoppingRepository repository;
    private ArchivedListDetailsContract.View view;
    private BaseSchedulerProvider schedulerProvider;
    private ShoppingList shoppingList;

    @Before
    public void setUp() {
        repository = Mockito.mock(ShoppingRepository.class);
        view = Mockito.mock(ArchivedListDetailsContract.View.class);
        schedulerProvider = new ImmediateSchedulerProvider();
        shoppingList = Mockito.mock(ShoppingList.class);
        when(shoppingList.getId()).thenReturn(SHOPPING_LIST_ID);
    }

    @Test
    public void subscribe_nonEmptyList_showItems() {
        List<ItemToBuyDb> listItems = new ArrayList<>();
        listItems.add(new ItemToBuyDb());
        when(repository.getListItems(SHOPPING_LIST_ID)).thenReturn(Flowable.just(listItems));
        ArchivedListDetailsPresenter presenter = createPresenter(repository);

        presenter.subscribe();

        verify(view).showItems(listItems);
    }

    @Test
    public void subscribe_emptyList_showItems() {
        when(repository.getListItems(SHOPPING_LIST_ID)).thenReturn(Flowable.just(new ArrayList<>()));
        ArchivedListDetailsPresenter presenter = createPresenter(repository);

        presenter.subscribe();

        verify(view).showItems(any());
    }

    @Test
    public void subscribe_error_showErrorView() {
        when(repository.getListItems(SHOPPING_LIST_ID)).thenReturn(Flowable.error(new Throwable()));
        ArchivedListDetailsPresenter presenter = createPresenter(repository);

        presenter.subscribe();

        verify(view).showErrorView(R.string.error_fetching_list_details);
    }

    private ArchivedListDetailsPresenter createPresenter(ShoppingRepository repository) {
        return new ArchivedListDetailsPresenter(view, repository, schedulerProvider, shoppingList);
    }

}