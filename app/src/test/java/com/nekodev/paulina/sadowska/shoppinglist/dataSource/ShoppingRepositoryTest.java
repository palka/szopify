package com.nekodev.paulina.sadowska.shoppinglist.dataSource;

import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */
public class ShoppingRepositoryTest {

    private ShoppingDataSource localDataSource;
    private List<ShoppingListDb> shoppingLists;

    @Before
    public void setUp() {
        localDataSource = Mockito.mock(ShoppingDataSource.class);
        shoppingLists = new ArrayList<>();
        shoppingLists.add(new ShoppingListDb());
        shoppingLists.add(new ShoppingListDb());
    }

    @Test
    public void getActiveLists_receivesList_returnsListAndSaveToCache() {
        when(localDataSource.getActiveLists()).thenReturn(Flowable.just(shoppingLists));
        ShoppingRepository repository = new ShoppingRepository(localDataSource);

        List<ShoppingListDb> result = new ArrayList<>();
        repository.getActiveLists().subscribe(result::addAll);

        assertEquals(shoppingLists.size(), result.size());
        assertEquals(shoppingLists.get(0), result.get(0));
        assertEquals(shoppingLists.size(), repository.mActiveListsCache.size());
        assertEquals(shoppingLists.get(0), repository.mActiveListsCache.get(0));
    }

    @Test
    public void getArchivedLists_receivesList_returnsListAndSaveToCache() {
        when(localDataSource.getArchivedLists()).thenReturn(Flowable.just(shoppingLists));
        ShoppingRepository repository = new ShoppingRepository(localDataSource);

        List<ShoppingListDb> result = new ArrayList<>();
        repository.getArchivedLists().subscribe(result::addAll);

        assertEquals(shoppingLists.size(), result.size());
        assertEquals(shoppingLists.get(0), result.get(0));
        assertEquals(shoppingLists.size(), repository.mArchivedListsCache.size());
        assertEquals(shoppingLists.get(0), repository.mArchivedListsCache.get(0));
    }

    @Test
    public void archiveList_returnsArchivedListAndModifyCache() {
        ShoppingListDb archivedList = new ShoppingListDb(99, "", 1L, false);
        when(localDataSource.archiveList(any())).thenReturn(Flowable.just(archivedList));
        ShoppingRepository repository = new ShoppingRepository(localDataSource);
        repository.mActiveListsCache.add(archivedList);
        repository.mArchivedListsCache.add(new ShoppingListDb());

        List<ShoppingListDb> result = new ArrayList<>();
        repository.archiveList(archivedList).subscribe(result::add);

        assertEquals(archivedList, result.get(0));
        assertEquals(2, repository.mArchivedListsCache.size());
        assertEquals(0, repository.mActiveListsCache.size());
    }

    @Test
    public void unarchiveList_returnsUnarchivedListAndModifyCache() {
        ShoppingListDb unarchivedList = new ShoppingListDb(99, "", 1L, true);
        when(localDataSource.unarchiveList(any())).thenReturn(Flowable.just(unarchivedList));
        ShoppingRepository repository = new ShoppingRepository(localDataSource);
        repository.mActiveListsCache.add(new ShoppingListDb());
        repository.mArchivedListsCache.add(unarchivedList);

        List<ShoppingListDb> result = new ArrayList<>();
        repository.unarchiveList(unarchivedList).subscribe(result::add);

        assertEquals(unarchivedList, result.get(0));
        assertEquals(0, repository.mArchivedListsCache.size());
        assertEquals(2, repository.mActiveListsCache.size());
    }

    @Test
    public void saveNewShoppingList_returnsNewListAndModifyCache() {
        ShoppingListDb listToSave = new ShoppingListDb();
        when(localDataSource.saveNewShoppingList(any())).thenReturn(Flowable.just(listToSave));
        ShoppingRepository repository = new ShoppingRepository(localDataSource);

        List<ShoppingListDb> result = new ArrayList<>();
        repository.saveNewShoppingList(listToSave).subscribe(result::add);

        assertEquals(listToSave, result.get(0));
        assertEquals(listToSave, repository.mActiveListsCache.get(0));
        assertEquals(0, repository.mArchivedListsCache.size());
        assertEquals(1, repository.mActiveListsCache.size());
    }

}