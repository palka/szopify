package com.nekodev.paulina.sadowska.shoppinglist.util.sort;

import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */
public class ModificationDateSortingStrategyTest {

    @Test
    public void sort_nonEmptyList_returnsSortedListDescendingDate() {
        List<ShoppingListDb> unsortedList = new ArrayList<>();
        unsortedList.add(createShoppingList(1, 12345L));
        unsortedList.add(createShoppingList(2, 1234567L));
        unsortedList.add(createShoppingList(3, 123456L));
        ModificationDateSortingStrategy strategy = new ModificationDateSortingStrategy();

        List<ShoppingListDb> sortedList = strategy.sort(unsortedList);

        assertTrue(sortedList.get(0).getId() == 2);
        assertTrue(sortedList.get(1).getId() == 3);
        assertTrue(sortedList.get(2).getId() == 1);
    }

    private ShoppingListDb createShoppingList(int id, long timestamp) {
        return new ShoppingListDb(id, "", timestamp, false);
    }

}