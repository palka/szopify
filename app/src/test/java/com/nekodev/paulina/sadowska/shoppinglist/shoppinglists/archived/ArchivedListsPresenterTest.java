package com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.archived;

import com.google.common.collect.Lists;
import com.nekodev.paulina.sadowska.shoppinglist.R;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingList;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListConverter;
import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepository;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.schedulers.ImmediateSchedulerProvider;
import com.nekodev.paulina.sadowska.shoppinglist.util.sort.SortingStrategy;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */
@RunWith(RobolectricTestRunner.class)
public class ArchivedListsPresenterTest {

    private ShoppingRepository repository;
    private ArchivedListsContract.View view;
    private BaseSchedulerProvider schedulerProvider;
    private ShoppingListConverter converter;
    private SortingStrategy sortingStrategy;

    @Before
    public void setUp() {
        repository = Mockito.mock(ShoppingRepository.class);
        view = Mockito.mock(ArchivedListsContract.View.class);
        schedulerProvider = new ImmediateSchedulerProvider();
        converter = Mockito.mock(ShoppingListConverter.class);
        when(converter.toFormattedShoppingList(any())).thenReturn(new ShoppingList());
        sortingStrategy = Lists::reverse;
    }

    @Test
    public void subscribe_nonEmptyList_showItems() {
        List<ShoppingListDb> lists = new ArrayList<>();
        lists.add(new ShoppingListDb());
        when(repository.getArchivedLists()).thenReturn(Flowable.just(lists));
        ArchivedListsContract.Presenter presenter = createPresenter(repository);

        presenter.subscribe();

        verify(view).showLists(any());
    }

    @Test
    public void subscribe_emptyList_showEmptyView() {
        when(repository.getArchivedLists()).thenReturn(Flowable.just(new ArrayList<>()));
        ArchivedListsContract.Presenter presenter = createPresenter(repository);

        presenter.subscribe();

        verify(view).showEmptyView();
    }

    @Test
    public void subscribe_error_showErrorView() {
        when(repository.getArchivedLists()).thenReturn(Flowable.error(new Throwable()));
        ArchivedListsContract.Presenter presenter = createPresenter(repository);

        presenter.subscribe();

        verify(view).showErrorView(R.string.error_fetching_lists);
    }


    @Test
    public void archiveItem_success_addShoppingListAtPosition() {
        int position = 876;
        ShoppingList archivedItem = new ShoppingList();
        when(repository.archiveList(any())).thenReturn(Flowable.just(new ShoppingListDb()));
        ArchivedListsContract.Presenter presenter = createPresenter(repository);

        presenter.archiveItem(archivedItem, position);

        verify(view).addShoppingListAtPosition(archivedItem, position);
        verify(view).hideEmptyView();
    }

    @Test
    public void unarchiveItem_notLastItem_removeAndShowSnackbar() {
        int position = 876;
        ShoppingList unarchivedItem = new ShoppingList();
        when(repository.unarchiveList(any())).thenReturn(Flowable.just(new ShoppingListDb()));
        ArchivedListsContract.Presenter presenter = createPresenter(repository);

        presenter.unarchiveItem(unarchivedItem, position, false);

        verify(view).removeItem(position);
        verify(view).showUndoSnackbar(unarchivedItem, position);
    }


    @Test
    public void unarchiveItem_lastItem_removeAndShowSnackbar() {
        int position = 876;
        ShoppingList unarchivedItem = new ShoppingList();
        when(repository.unarchiveList(any())).thenReturn(Flowable.just(new ShoppingListDb()));
        ArchivedListsContract.Presenter presenter = createPresenter(repository);

        presenter.unarchiveItem(unarchivedItem, position, true);

        verify(view).removeItem(position);
        verify(view).showUndoSnackbar(unarchivedItem, position);
        verify(view).showEmptyView();
    }

    @Test
    public void onListClicked_openListDetailsCalled() {
        ShoppingList shoppingList = Mockito.mock(ShoppingList.class);
        ArchivedListsPresenter presenter = createPresenter(repository);
        presenter.onListClicked(shoppingList);
        verify(view).showListDetails(shoppingList);
    }

    private ArchivedListsPresenter createPresenter(ShoppingRepository repository) {
        return new ArchivedListsPresenter(view, repository, schedulerProvider, converter, sortingStrategy);
    }
}