package com.nekodev.paulina.sadowska.shoppinglist;

import com.nekodev.paulina.sadowska.shoppinglist.data.ShoppingListDb;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepository;
import com.nekodev.paulina.sadowska.shoppinglist.dataSource.ShoppingRepositoryComponent;

import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */

public class MockAppComponent implements ShoppingRepositoryComponent {

    static final String FIRST_SHOPPING_LIST_NAME = "test123";

    @Override
    public ShoppingRepository getShoppingRepository() {
        ShoppingRepository repository = Mockito.mock(ShoppingRepository.class);

        when(repository.getActiveLists()).thenReturn(Flowable.just(createShoppingLists()));

        return repository;
    }

    private List<ShoppingListDb> createShoppingLists() {
        List<ShoppingListDb> shoppingLists = new ArrayList<>();
        shoppingLists.add(new ShoppingListDb(99, FIRST_SHOPPING_LIST_NAME, 9L, false));
        return shoppingLists;
    }
}
