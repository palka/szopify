package com.nekodev.paulina.sadowska.shoppinglist;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.nekodev.paulina.sadowska.shoppinglist.shoppinglists.active.ActiveListsActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Paulina Sadowska on 20.10.2017.
 */
@RunWith(AndroidJUnit4.class)
public class ActiveListsViewTest {


    @Rule
    public ActivityTestRule<ActiveListsActivity> activityRule
            = new ActivityTestRule<>(
            ActiveListsActivity.class,
            true,
            false);

    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        ShoppingListApplication app
                = (ShoppingListApplication) instrumentation.getTargetContext().getApplicationContext();
        app.setAppComponent(new MockAppComponent());
        activityRule.launchActivity(new Intent());
    }

    @Test
    public void fabClicked_alertDialogDisplayed() {
        onView(withId(R.id.active_lists_fab))
                .perform(click());
        onView(withText(R.string.add_new_list_title))
                .check(matches(isDisplayed()));
    }
}